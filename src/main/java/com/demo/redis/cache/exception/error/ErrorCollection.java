package com.demo.redis.cache.exception.error;

import lombok.Getter;
import lombok.NoArgsConstructor;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collection;

@Getter
@NoArgsConstructor
public class ErrorCollection implements Serializable {

	private static final long serialVersionUID = -511214055167796590L;

	private final Collection<CacheError> cacheErrors = new ArrayList<>();

	public ErrorCollection(final CacheError cacheError) {
		super();
		addError(cacheError);
	}

	public ErrorCollection(final Collection<CacheError> cacheErrors) {
		super();
		addErrors(cacheErrors);
	}

	public void addError(final CacheError cacheError) {
		this.cacheErrors.add(cacheError);
	}

	public void addErrors(final Collection<CacheError> cacheErrors) {
		this.cacheErrors.addAll(cacheErrors);
	}

}
