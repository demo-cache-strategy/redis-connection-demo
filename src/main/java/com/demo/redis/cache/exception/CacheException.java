package com.demo.redis.cache.exception;

import com.demo.redis.cache.exception.error.CacheError;
import com.demo.redis.cache.exception.error.ErrorCollection;
import lombok.Getter;

import java.util.Arrays;
import java.util.stream.Collectors;

@Getter
public class CacheException extends RuntimeException {

	private static final long serialVersionUID = 2511898525250120134L;

	private final String code;

	private final int responseCode;

	private final ErrorCollection errorCollection;

	public CacheException(final String code, final int responseCode, final String message,
						  final ErrorCollection errorCollection) {
		super(message);
		this.code = code;
		this.responseCode = responseCode;
		this.errorCollection = errorCollection;
	}

	public CacheException(final String code, final int responseCode, final String message,
						  final CacheError snwError) {
		this(code, responseCode, message, new ErrorCollection(snwError));
	}

	public CacheException(final String code, final int responseCode, final String message,
						  final CacheError... snwErrors) {
		this(code, responseCode, message, new ErrorCollection(Arrays.stream(snwErrors).collect(Collectors.toList())));
	}

}
