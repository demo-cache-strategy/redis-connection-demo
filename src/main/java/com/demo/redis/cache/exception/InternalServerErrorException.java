package com.demo.redis.cache.exception;

import org.springframework.http.HttpStatus;

public class InternalServerErrorException extends CacheException {

	private static final long serialVersionUID = -4985693228110224616L;

	public InternalServerErrorException(final String code, final String message) {
		super(code, HttpStatus.INTERNAL_SERVER_ERROR.value(), message);
	}

}
