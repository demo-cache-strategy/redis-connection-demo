package com.demo.redis.cache.exception;

import com.demo.redis.cache.exception.error.ErrorCollection;
import org.springframework.http.HttpStatus;

public class NotFoundException extends CacheException {

	private static final long serialVersionUID = 1147677766584826705L;

	public NotFoundException(final String code, final String message) {
		super(code, HttpStatus.NOT_FOUND.value(), message);
	}

	public NotFoundException(final String code, final String message, final ErrorCollection data) {
		super(code, HttpStatus.NOT_FOUND.value(), message, data);
	}
}
