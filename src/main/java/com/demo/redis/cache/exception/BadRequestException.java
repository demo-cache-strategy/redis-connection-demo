package com.demo.redis.cache.exception;

import com.demo.redis.cache.exception.error.CacheError;
import com.demo.redis.cache.exception.error.ErrorCollection;
import com.demo.redis.cache.util.constant.ExceptionConstants;
import org.springframework.http.HttpStatus;

public class BadRequestException extends CacheException {
	private static final long serialVersionUID = -4678495481421270851L;

	public BadRequestException(final ErrorCollection data) {
		super(ExceptionConstants.CODE_BAD_REQUEST, HttpStatus.BAD_REQUEST.value(), ExceptionConstants.MESSAGE_BAD_REQUEST, data);
	}

	public BadRequestException(final CacheError cacheError) {
		super(ExceptionConstants.CODE_BAD_REQUEST, HttpStatus.BAD_REQUEST.value(), ExceptionConstants.MESSAGE_BAD_REQUEST, cacheError);
	}

	public BadRequestException(final CacheError... cacheError) {
		super(ExceptionConstants.CODE_BAD_REQUEST, HttpStatus.BAD_REQUEST.value(), ExceptionConstants.MESSAGE_BAD_REQUEST, cacheError);
	}}
