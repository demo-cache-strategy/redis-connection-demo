package com.demo.redis.cache.util.constant;

public class ExceptionConstants {

	// GENERIC CODES
	public static final String CODE_INTERNAL_SERVER_ERROR = "SPRO-500";
	public static final String CODE_NOT_FOUND = "SPRO-404";
	public static final String CODE_BAD_REQUEST = "SPRO-400";
	public static final String CODE_CONFLICT = "SPRO-409";

	// GENERIC MESSAGES
	public static final String MESSAGE_INTERNAL_SERVER_ERROR = "INTERNAL SERVER ERROR";
	public static final String MESSAGE_BAD_REQUEST = "BAD REQUEST - The input was wrong";
	public static final String MESSAGE_NOT_FOUND = "NOT FOUND - Resource not found";
	public static final String MESSAGE_CONFLICT = "NOT FOUND - Resource not found";
	public static final String MESSAGE_BAD_REQUEST_EMPTY = "BAD REQUEST - The input can neither be empty nor null";

	private ExceptionConstants() {
		throw new IllegalStateException("Utility Class");
	}

}
