package com.demo.redis.cache.util.constant;

public class RestConstants {

    //General Resources
    public static final String APPLICATION_NAME = "/cache-api";
    public static final String API_VERSION_1 = "/v1";

    //Especific Resources
    public static final String RESOURCE_USERS = "/users";
    public static final String RESOURCE_ROLES = "/roles";

    private RestConstants() {
        throw new IllegalStateException("Utility Class");
    }

}