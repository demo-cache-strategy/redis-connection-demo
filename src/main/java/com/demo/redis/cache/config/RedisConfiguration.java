package com.demo.redis.cache.config;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.data.redis.connection.RedisStandaloneConfiguration;
import org.springframework.data.redis.connection.jedis.JedisConnectionFactory;

@Configuration
class RedisConfiguration {

    @Bean
    public JedisConnectionFactory redisConnectionFactory() {

        /*0.0.0.0 Docker Local*/
        RedisStandaloneConfiguration config = new RedisStandaloneConfiguration("localhost", 6379);

        /*34.77.96.162:6379 Google Cloud*/
//        RedisStandaloneConfiguration config = new RedisStandaloneConfiguration("34.77.96.162", 6379);

//        config.setPassword("");
        return new JedisConnectionFactory(config);
    }
}