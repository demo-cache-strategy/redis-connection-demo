package com.demo.redis.cache.controller;

import com.demo.redis.cache.controller.json.CacheResponse;
import com.demo.redis.cache.controller.json.UserJson;

public interface UserController {

    CacheResponse<UserJson[]> findAllUser();
    CacheResponse<UserJson> findById(Integer id);
    CacheResponse<UserJson> findByName(String name);
    CacheResponse<UserJson> findByUsername(String username);
    CacheResponse<UserJson> addUser(UserJson userJson);

}
