package com.demo.redis.cache.controller.mapper;

import com.demo.redis.cache.controller.json.RoleJson;
import com.demo.redis.cache.service.cache.dto.RoleDto;
import org.springframework.stereotype.Component;

import java.util.Collection;
import java.util.List;
import java.util.stream.Collectors;

@Component
public class RoleControllerMapper {

    public RoleJson mapToJson(final RoleDto dto) {
        if (null == dto) {
            return null;
        }

        final RoleJson json = new RoleJson();
        json.setId(dto.getId());
        json.setName(dto.getName());
        json.setUsers(dto.getUsers());

        return json;
    }

    public RoleDto mapToDto(final RoleJson json) {
        if (null == json) {
            return null;
        }

        final RoleDto dto = new RoleDto();
        dto.setId(json.getId());
        dto.setName(json.getName());
        dto.setUsers(json.getUsers());

        return dto;
    }

    public RoleJson[] mapToJsonList(final Collection<RoleDto> dtoList) {
        return dtoList.stream().map(this::mapToJson).toArray(RoleJson[]::new);
    }

    public List<RoleDto> mapToDtoList(final Collection<RoleJson> jsonList) {
        return jsonList.stream().map(this::mapToDto).collect(Collectors.toList());
    }

}
