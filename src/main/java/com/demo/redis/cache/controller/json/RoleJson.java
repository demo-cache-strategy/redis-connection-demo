package com.demo.redis.cache.controller.json;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;
import java.util.List;

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class RoleJson implements Serializable {
    private Integer id;
    private String name;
    private List<Integer> users;
}
