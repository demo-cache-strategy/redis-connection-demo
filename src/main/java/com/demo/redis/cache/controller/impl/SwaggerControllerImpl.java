package com.demo.redis.cache.controller.impl;

import com.demo.redis.cache.controller.SwaggerController;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import springfox.documentation.annotations.ApiIgnore;

@ApiIgnore
@Controller
public class SwaggerControllerImpl implements SwaggerController {

    @Override
    @GetMapping(value = { "/", "/swagger" })
    public String swaggerHome() {
        return "redirect:swagger-ui.html";
    }
}
