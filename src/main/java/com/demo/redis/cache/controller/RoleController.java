package com.demo.redis.cache.controller;

import com.demo.redis.cache.controller.json.CacheResponse;
import com.demo.redis.cache.controller.json.RoleJson;

public interface RoleController {
    CacheResponse<RoleJson[]> findAllRole();
    CacheResponse<RoleJson> findById(Integer id);
    CacheResponse<RoleJson> findByName(String name);
    CacheResponse<RoleJson> addRole(RoleJson userJson);
   CacheResponse<RoleJson> deleteById(Integer id);
}
