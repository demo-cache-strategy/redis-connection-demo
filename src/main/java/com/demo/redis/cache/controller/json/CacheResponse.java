package com.demo.redis.cache.controller.json;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class CacheResponse<T extends Serializable> implements Serializable {

    private String status;
    private String code;
    private String message;
    private T data;

    public CacheResponse(final String status, final String code, final String message) {
        this.status = status;
        this.code = code;
        this.message = message;
    }

}