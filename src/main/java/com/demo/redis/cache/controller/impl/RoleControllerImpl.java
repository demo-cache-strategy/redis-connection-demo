package com.demo.redis.cache.controller.impl;

import com.demo.redis.cache.controller.RoleController;
import com.demo.redis.cache.controller.json.CacheResponse;
import com.demo.redis.cache.controller.json.RoleJson;
import com.demo.redis.cache.controller.mapper.RoleControllerMapper;
import com.demo.redis.cache.exception.error.ErrorCollection;
import com.demo.redis.cache.service.cache.RoleService;
import com.demo.redis.cache.service.cache.dto.RoleDto;
import com.demo.redis.cache.util.constant.CommonConstants;
import com.demo.redis.cache.util.constant.RestConstants;
import io.swagger.annotations.*;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;

@Api(tags = "Roles")
@RestController("roleController")
@RequestMapping(value = RestConstants.APPLICATION_NAME + RestConstants.API_VERSION_1)
@RequiredArgsConstructor
public class RoleControllerImpl implements RoleController {

    private final RoleControllerMapper roleControllerMapper;
    private final RoleService roleService;

    @ApiOperation(value = "Return Roles by country code", notes = "Return an information list associated to the Role: id and name for sale.", response = CacheResponse.class)
    @ApiResponses({
            @ApiResponse(code = 200, message = "Role found", response = RoleJson.class),
            @ApiResponse(code = 500, message = "Internal server error", response = ErrorCollection.class)
    })
    @GetMapping(RestConstants.RESOURCE_ROLES)
    @ResponseBody
    @ResponseStatus(HttpStatus.OK)
    @Override
    public CacheResponse<RoleJson[]> findAllRole() {
        CacheResponse<RoleJson[]> cacheResponse = new CacheResponse<>();

        cacheResponse.setStatus(CommonConstants.SUCCESS);
        cacheResponse.setCode(String.valueOf(HttpStatus.OK.value()));
        cacheResponse.setMessage(CommonConstants.OK);
        cacheResponse.setData(roleControllerMapper.mapToJsonList(roleService.findAllRoles()));

        return cacheResponse;
    }

    @ApiOperation(value = "Return Role by id", notes = "Return an information associated to the Role", response = CacheResponse.class)
    @ApiResponses({
            @ApiResponse(code = 200, message = "Role found", response = RoleJson.class),
            @ApiResponse(code = 404, message = "Role not found", response = ErrorCollection.class),
            @ApiResponse(code = 500, message = "Internal server error", response = ErrorCollection.class)
    })
    @GetMapping(RestConstants.RESOURCE_ROLES + "/{id}")
    @ResponseBody
    @ResponseStatus(HttpStatus.OK)
    @Override
    public CacheResponse<RoleJson> findById(@PathVariable(value = "id") final Integer id) {
        final CacheResponse<RoleJson> cacheResponse = new CacheResponse<>();
        cacheResponse.setStatus(CommonConstants.SUCCESS);
        cacheResponse.setCode(String.valueOf(HttpStatus.OK.value()));
        cacheResponse.setMessage(CommonConstants.OK);

        final RoleDto roleDto = RoleDto.builder().id(id).build();

        cacheResponse.setData(roleControllerMapper.mapToJson(roleService.findById(roleDto)));
        return cacheResponse;
    }

    @ApiOperation(value = "Return Role by name", notes = "Return an information associated to the Role", response = CacheResponse.class)
    @ApiResponses({
            @ApiResponse(code = 200, message = "Role found", response = RoleJson.class),
            @ApiResponse(code = 404, message = "Role not found", response = ErrorCollection.class),
            @ApiResponse(code = 500, message = "Internal server error", response = ErrorCollection.class)
    })
    @GetMapping(RestConstants.RESOURCE_ROLES + "/name/{name}")
    @ResponseBody
    @ResponseStatus(HttpStatus.OK)
    @Override
    public CacheResponse<RoleJson> findByName(@PathVariable(value = "name") final String name) {
        final CacheResponse<RoleJson> cacheResponse = new CacheResponse<>();
        cacheResponse.setStatus(CommonConstants.SUCCESS);
        cacheResponse.setCode(String.valueOf(HttpStatus.OK.value()));
        cacheResponse.setMessage(CommonConstants.OK);

        final RoleDto roleDto = RoleDto.builder().name(name).build();

        cacheResponse.setData(roleControllerMapper.mapToJson(roleService.findByName(roleDto)));
        return cacheResponse;
    }

    @ApiOperation(value = "Create a new role")
    @ApiParam(value = "Role information")
    @ApiResponses({ @ApiResponse(code = 200, message = "Role added", response = RoleJson.class),
            @ApiResponse(code = 400, message = "General service error", response = ErrorCollection.class) })
    @PostMapping(path = RestConstants.RESOURCE_ROLES, produces = "application/json", consumes = "application/json")
    @ResponseBody
    @ResponseStatus(HttpStatus.CREATED)
    @Override
    public CacheResponse<RoleJson> addRole(@Valid @RequestBody final RoleJson roleJson) {
        final CacheResponse<RoleJson> cacheResponse = new CacheResponse<>();
        cacheResponse.setStatus(CommonConstants.SUCCESS);
        cacheResponse.setCode(String.valueOf(HttpStatus.OK.value()));
        cacheResponse.setMessage(CommonConstants.OK);

        final RoleDto roleDto = roleControllerMapper.mapToDto(roleJson);

        cacheResponse.setData(roleControllerMapper.mapToJson(roleService.save(roleDto)));
        return cacheResponse;
    }

    @ApiOperation(value = "Return Role by id", notes = "Return an information associated to the Role", response = CacheResponse.class)
    @ApiResponses({
            @ApiResponse(code = 200, message = "Role found", response = RoleJson.class),
            @ApiResponse(code = 404, message = "Role not found", response = ErrorCollection.class),
            @ApiResponse(code = 500, message = "Internal server error", response = ErrorCollection.class)
    })
    @DeleteMapping(RestConstants.RESOURCE_ROLES + "/{id}")
    @ResponseBody
    @ResponseStatus(HttpStatus.OK)
    @Override
    public CacheResponse<RoleJson> deleteById(@PathVariable(value = "id") final Integer id) {
        final CacheResponse<RoleJson> cacheResponse = new CacheResponse<>();
        cacheResponse.setStatus(CommonConstants.SUCCESS);
        cacheResponse.setCode(String.valueOf(HttpStatus.OK.value()));
        cacheResponse.setMessage(CommonConstants.OK);

        final RoleDto roleDto = RoleDto.builder().id(id).build();

        cacheResponse.setData(roleControllerMapper.mapToJson(roleService.delete(roleDto)));
        return cacheResponse;
    }
}
