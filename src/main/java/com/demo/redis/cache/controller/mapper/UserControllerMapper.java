package com.demo.redis.cache.controller.mapper;

import com.demo.redis.cache.controller.json.UserJson;
import com.demo.redis.cache.service.cache.dto.UserDto;
import org.springframework.stereotype.Component;

import java.util.Collection;
import java.util.List;
import java.util.stream.Collectors;

@Component
public class UserControllerMapper {

    public UserJson mapToJson(final UserDto dto) {
        if (null == dto) {
            return null;
        }

        final UserJson json = new UserJson();

        json.setId(dto.getId());
        json.setName(dto.getName());
        json.setUsername(dto.getUsername());
        json.setPassword(dto.getPassword());
        json.setMail(dto.getMail());
        json.setStatus(dto.getStatus());
        json.setRoles(dto.getRoles());

        return json;
    }

    public UserDto mapToDto(final UserJson json) {
        if (null == json) {
            return null;
        }

        final UserDto dto = new UserDto();

        dto.setId(json.getId());
        dto.setName(json.getName());
        dto.setUsername(json.getUsername());
        dto.setPassword(json.getPassword());
        dto.setMail(json.getMail());
        dto.setStatus(json.getStatus());
        dto.setRoles(json.getRoles());

        return dto;
    }

    public UserJson[] mapToJsonList(final Collection<UserDto> dtoList) {
        return dtoList.stream().map(this::mapToJson).toArray(UserJson[]::new);
    }

    public List<UserDto> mapToDtoList(final Collection<UserJson> jsonList) {
        return jsonList.stream().map(this::mapToDto).collect(Collectors.toList());
    }

}
