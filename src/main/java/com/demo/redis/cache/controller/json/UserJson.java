package com.demo.redis.cache.controller.json;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.data.redis.core.RedisHash;

import java.io.Serializable;
import java.util.List;

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class UserJson implements Serializable {
    private Integer id;
    private String name;
    private String username;
    private String password;
    private String status;
    private String mail;
    private List<Integer> roles;
}
