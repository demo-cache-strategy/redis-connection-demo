package com.demo.redis.cache.controller.impl;

import com.demo.redis.cache.controller.UserController;
import com.demo.redis.cache.controller.json.CacheResponse;
import com.demo.redis.cache.controller.json.UserJson;
import com.demo.redis.cache.controller.mapper.UserControllerMapper;
import com.demo.redis.cache.exception.error.ErrorCollection;
import com.demo.redis.cache.service.cache.UserService;
import com.demo.redis.cache.service.cache.dto.UserDto;
import com.demo.redis.cache.util.constant.CommonConstants;
import com.demo.redis.cache.util.constant.RestConstants;
import io.swagger.annotations.*;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;

@Api(tags = "Users")
@RestController("userController")
@RequestMapping(value = RestConstants.APPLICATION_NAME + RestConstants.API_VERSION_1)
@RequiredArgsConstructor
public class UserControllerImpl implements UserController {

    @Autowired
    private final UserControllerMapper userControllerMapper;
    @Autowired
    private final UserService userService;

    @ApiOperation(value = "Return Users", notes = "Return an user information list", response = CacheResponse.class)
    @ApiResponses({
            @ApiResponse(code = 200, message = "Users found", response = UserJson.class),
            @ApiResponse(code = 500, message = "Internal server error", response = ErrorCollection.class)
    })
    @GetMapping(RestConstants.RESOURCE_USERS)
    @ResponseBody
    @ResponseStatus(HttpStatus.OK)
    @Override
    public CacheResponse<UserJson[]> findAllUser() {
        CacheResponse<UserJson[]> cacheResponse = new CacheResponse<>();

        cacheResponse.setStatus(CommonConstants.SUCCESS);
        cacheResponse.setCode(String.valueOf(HttpStatus.OK.value()));
        cacheResponse.setMessage(CommonConstants.OK);
        cacheResponse.setData(userControllerMapper.mapToJsonList(userService.findAll()));

        return cacheResponse;
    }

    @ApiOperation(value = "Return User by id", notes = "Return an information associated to the User", response = CacheResponse.class)
    @ApiResponses({
            @ApiResponse(code = 200, message = "User found", response = UserJson.class),
            @ApiResponse(code = 404, message = "User not found", response = ErrorCollection.class),
            @ApiResponse(code = 500, message = "Internal server error", response = ErrorCollection.class)
    })
    @GetMapping(RestConstants.RESOURCE_USERS + "/{id}")
    @ResponseBody
    @ResponseStatus(HttpStatus.OK)
    @Override
    public CacheResponse<UserJson> findById(@PathVariable(value = "id") final Integer id) {
        final CacheResponse<UserJson> cacheResponse = new CacheResponse<>();
        cacheResponse.setStatus(CommonConstants.SUCCESS);
        cacheResponse.setCode(String.valueOf(HttpStatus.OK.value()));
        cacheResponse.setMessage(CommonConstants.OK);

        final UserDto userDto = UserDto.builder().id(id).build();

        cacheResponse.setData(userControllerMapper.mapToJson(userService.findById(userDto)));
        return cacheResponse;
    }

    @ApiOperation(value = "Return User by name", notes = "Return an information associated to the User", response = CacheResponse.class)
    @ApiResponses({
            @ApiResponse(code = 200, message = "User found", response = UserJson.class),
            @ApiResponse(code = 404, message = "User not found", response = ErrorCollection.class),
            @ApiResponse(code = 500, message = "Internal server error", response = ErrorCollection.class)
    })
    @GetMapping(RestConstants.RESOURCE_USERS + "/name/{name}")
    @ResponseBody
    @ResponseStatus(HttpStatus.OK)
    @Override
    public CacheResponse<UserJson> findByName(@PathVariable(value = "name") final String name) {
        final CacheResponse<UserJson> cacheResponse = new CacheResponse<>();
        cacheResponse.setStatus(CommonConstants.SUCCESS);
        cacheResponse.setCode(String.valueOf(HttpStatus.OK.value()));
        cacheResponse.setMessage(CommonConstants.OK);

        final UserDto userDto = UserDto.builder().name(name).build();

        cacheResponse.setData(userControllerMapper.mapToJson(userService.findByName(userDto)));
        return cacheResponse;
    }

    @ApiOperation(value = "Return User by username", notes = "Return an information associated to the User", response = CacheResponse.class)
    @ApiResponses({
            @ApiResponse(code = 200, message = "User found", response = UserJson.class),
            @ApiResponse(code = 404, message = "User not found", response = ErrorCollection.class),
            @ApiResponse(code = 500, message = "Internal server error", response = ErrorCollection.class)
    })
    @GetMapping(RestConstants.RESOURCE_USERS + "/username/{username}")
    @ResponseBody
    @ResponseStatus(HttpStatus.OK)
    @Override
    public CacheResponse<UserJson> findByUsername(@PathVariable(value = "username") final String username) {
        final CacheResponse<UserJson> cacheResponse = new CacheResponse<>();
        cacheResponse.setStatus(CommonConstants.SUCCESS);
        cacheResponse.setCode(String.valueOf(HttpStatus.OK.value()));
        cacheResponse.setMessage(CommonConstants.OK);

        final UserDto userDto = UserDto.builder().username(username).build();

        cacheResponse.setData(userControllerMapper.mapToJson(userService.findByUsername(userDto)));
        return cacheResponse;
    }

    @ApiOperation(value = "Create a new role")
    @ApiParam(value = "User information")
    @ApiResponses({ @ApiResponse(code = 200, message = "User added", response = UserJson.class),
            @ApiResponse(code = 400, message = "General service error", response = ErrorCollection.class) })
    @PostMapping(path = RestConstants.RESOURCE_USERS, produces = "application/json", consumes = "application/json")
    @ResponseBody
    @ResponseStatus(HttpStatus.CREATED)
    @Override
    public CacheResponse<UserJson> addUser(@Valid @RequestBody final UserJson userJson) {
        final CacheResponse<UserJson> cacheResponse = new CacheResponse<>();
        cacheResponse.setStatus(CommonConstants.SUCCESS);
        cacheResponse.setCode(String.valueOf(HttpStatus.OK.value()));
        cacheResponse.setMessage(CommonConstants.OK);

        final UserDto userDto = userControllerMapper.mapToDto(userJson);

        cacheResponse.setData(userControllerMapper.mapToJson(userService.save(userDto)));
        return cacheResponse;
    }
}
