package com.demo.redis.cache.service.cache.impl;

import com.demo.redis.cache.exception.NotFoundException;
import com.demo.redis.cache.persistence.cache.entity.Role;
import com.demo.redis.cache.persistence.cache.repository.RoleRepository;
import com.demo.redis.cache.service.cache.RoleService;
import com.demo.redis.cache.service.cache.dto.RoleDto;
import com.demo.redis.cache.service.cache.mapper.RoleCacheServiceMapper;
import com.demo.redis.cache.service.cache.mapper.RoleCacheStorageMapper;
import com.demo.redis.cache.service.storage.StorageRoleService;
import com.demo.redis.cache.util.constant.ExceptionConstants;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.StreamSupport;

@Service
@RequiredArgsConstructor
public class RoleServiceImpl implements RoleService {

    private final RoleRepository cacheRoleRepository;
    private final RoleCacheServiceMapper roleCacheServiceMapper;
    private final StorageRoleService storageRoleService;
    private final RoleCacheStorageMapper roleCacheStorageMapper;

    @Override
    public List<RoleDto> findAllRoles() {
        List<Role> roleList = StreamSupport.stream(cacheRoleRepository.findAll().spliterator(), false).collect(Collectors.toList());

        if (!roleList.isEmpty()) {
            List<RoleDto> roleDtoList = roleCacheStorageMapper.mapToCacheDtoList(storageRoleService.findAll());
            roleDtoList.forEach(this::saveRoleInCache);
            return roleDtoList;
        }

        return roleCacheServiceMapper.mapToDtoList(roleList);
    }

    @Override
    public RoleDto findById(RoleDto roleDto){
        RoleDto roleDtoResult;

        if(!cacheRoleRepository.findById(roleDto.getId().toString()).isPresent()){
            roleDtoResult = saveRoleInCache(roleCacheStorageMapper.mapToCacheDto(storageRoleService.findById(roleDto.getId())));
        } else {
            roleDtoResult = roleCacheServiceMapper.mapToDto(cacheRoleRepository.findById(roleDto.getId().toString()).orElseThrow(() -> new NotFoundException("404", ExceptionConstants.MESSAGE_NOT_FOUND)));
        }

        return roleDtoResult;
    }

    @Override
    public RoleDto findByName(RoleDto roleDto){
        RoleDto roleDtoResult;

        if(!cacheRoleRepository.findById(roleDto.getId().toString()).isPresent()){
            roleDtoResult = saveRoleInCache(roleCacheStorageMapper.mapToCacheDto(storageRoleService.findByName(roleDto.getName())));
        } else {
            roleDtoResult = roleCacheServiceMapper.mapToDto(cacheRoleRepository.findByName(roleDto.getName()).orElseThrow(() -> new NotFoundException("404", ExceptionConstants.MESSAGE_NOT_FOUND)));
        }

        return roleDtoResult;
    }

    public RoleDto save(RoleDto roleDto){
        return saveRoleInStorage(saveRoleInCache(roleDto));
    }

    private RoleDto saveRoleInStorage(RoleDto roleDto) {
        return roleCacheStorageMapper.mapToCacheDto(storageRoleService.save(roleCacheStorageMapper.mapToStorageDto(roleDto)));
    }

    private RoleDto saveRoleInCache(RoleDto roleDto) {
        return roleCacheServiceMapper.mapToDto(cacheRoleRepository.save(roleCacheServiceMapper.mapToEntity(roleDto)));
    }

    @Override
    public RoleDto delete(RoleDto roleDto) {

        cacheRoleRepository.deleteById(roleDto.getId().toString());

        return deleteRoleInStorage(roleDto);
    }

    private RoleDto deleteRoleInStorage(RoleDto roleDto) {
        return roleCacheStorageMapper.mapToCacheDto(storageRoleService.delete(roleCacheStorageMapper.mapToStorageDto(roleDto)));
    }
}
