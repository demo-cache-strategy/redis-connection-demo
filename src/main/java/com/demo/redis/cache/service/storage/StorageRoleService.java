package com.demo.redis.cache.service.storage;

import com.demo.redis.cache.service.storage.dto.RoleDto;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

public interface StorageRoleService {

	List<RoleDto> findAll();
	RoleDto findById(Integer id);
	RoleDto findByName(final String name);
	RoleDto save(RoleDto roleDto);
	RoleDto update(RoleDto roleDto);
	RoleDto delete(RoleDto roleDto);

}
