package com.demo.redis.cache.service.cache.impl;

import com.demo.redis.cache.exception.NotFoundException;
import com.demo.redis.cache.persistence.cache.entity.User;
import com.demo.redis.cache.persistence.cache.repository.UserRepository;
import com.demo.redis.cache.service.cache.UserService;
import com.demo.redis.cache.service.cache.dto.UserDto;
import com.demo.redis.cache.service.cache.mapper.UserCacheServiceMapper;
import com.demo.redis.cache.util.constant.ExceptionConstants;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service
@RequiredArgsConstructor
public class UserServiceImpl implements UserService {

    private final UserRepository userRepository;
    private final UserCacheServiceMapper userCacheServiceMapper;

    @Override
    public List<UserDto> findAll() {
        List<User> userList = new ArrayList<>();
        userRepository.findAll().forEach(userList::add);
        return userCacheServiceMapper.mapToDtoList(userList);
    }

    @Override
    public UserDto findById(UserDto userDto){
        User user = userCacheServiceMapper.mapToEntity(userDto);
        UserDto userDtoSaved = userCacheServiceMapper.mapToDto(userRepository.findById(userDto.getId().toString()).orElseThrow(() -> new NotFoundException("404", ExceptionConstants.MESSAGE_NOT_FOUND)));
        return userDtoSaved;
    }

    @Override
    public UserDto findByName(UserDto userDto){
        User user = userCacheServiceMapper.mapToEntity(userDto);
        UserDto userDtoSaved = userCacheServiceMapper.mapToDto(userRepository.findByName(userDto.getName()).orElseThrow(() -> new NotFoundException("404", ExceptionConstants.MESSAGE_NOT_FOUND)));
        return userDtoSaved;
    }

    @Override
    public UserDto findByUsername(UserDto userDto){
        User user = userCacheServiceMapper.mapToEntity(userDto);
        UserDto userDtoSaved = userCacheServiceMapper.mapToDto(userRepository.findByUsername(userDto.getUsername()).orElseThrow(() -> new NotFoundException("404", ExceptionConstants.MESSAGE_NOT_FOUND)));
        return userDtoSaved;
    }

    @Override
    public UserDto save(UserDto userDto){
        User user = userCacheServiceMapper.mapToEntity(userDto);
        UserDto userDtoSaved = userCacheServiceMapper.mapToDto(userRepository.save(user));
        return userDtoSaved;
    }
}
