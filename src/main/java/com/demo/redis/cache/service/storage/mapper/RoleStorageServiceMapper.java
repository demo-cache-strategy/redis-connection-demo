package com.demo.redis.cache.service.storage.mapper;

import com.demo.redis.cache.persistence.storage.entity.Role;
import com.demo.redis.cache.persistence.storage.entity.User;
import com.demo.redis.cache.service.storage.dto.RoleDto;
import com.demo.redis.cache.service.storage.dto.UserDto;
import org.springframework.stereotype.Component;

import java.util.*;
import java.util.stream.Collectors;

@Component
public class RoleStorageServiceMapper {

	public Role mapToEntity(final RoleDto roleDto) {
		final Role role = new Role();

		if (null != roleDto) {
			if (null == roleDto.getId() || roleDto.getId() <= 0) {
				role.setId(null);
			} else {
				role.setId(roleDto.getId());
			}

			role.setName(roleDto.getName());

			role.setUsers(getUserList(roleDto));
		}

		return role;
	}

	private Set<User> getUserList(final RoleDto roleDto) {
		final Set<User> listUserDto = new HashSet<>();

		if (null != roleDto.getUsers() && !roleDto.getUsers().isEmpty()) {
			roleDto.getUsers().stream().forEach(userDto -> {
				final User user = new User();
				user.setId(userDto.getId());
				user.setName(userDto.getName());
				user.setUsername(userDto.getUsername());
				user.setPassword(userDto.getPassword());
				user.setMail(userDto.getMail());
				listUserDto.add(user);
			});
		}
		return listUserDto;
	}

	public RoleDto mapToDto(final Role role) {
		final RoleDto roleDto = new RoleDto();

		if (null != role) {
			roleDto.setId(role.getId());
			roleDto.setName(role.getName());
			if (null != role.getUsers()) {
				roleDto.setUsers(getUserDtoList(role));
			}
		}
		return roleDto;
	}

	private List<UserDto> getUserDtoList(final Role role) {
		final List<UserDto> listUserDto = new ArrayList<>();

		if (null != role.getUsers()) {
			role.getUsers().stream().forEach(user -> {
				final UserDto userDto = new UserDto();
				userDto.setId(user.getId());
				userDto.setName(user.getName());
				userDto.setUsername(user.getUsername());
				userDto.setPassword(user.getPassword());
				userDto.setMail(user.getMail());
				listUserDto.add(userDto);
			});
		}
		return listUserDto;
	}

	public Set<Role> mapToEntityList(final Collection<RoleDto> roleDtoList) {
		if (roleDtoList == null) {
			return Collections.emptySet();
		}

		return roleDtoList.stream().map(this::mapToEntity).collect(Collectors.toSet());
	}

	public List<RoleDto> mapToDtoList(final Collection<Role> roles) {
		if (roles == null) {
			return Collections.emptyList();
		}

		return roles.stream().map(this::mapToDto).collect(Collectors.toList());
	}

}
