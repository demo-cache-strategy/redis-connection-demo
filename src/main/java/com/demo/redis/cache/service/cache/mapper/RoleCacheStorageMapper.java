package com.demo.redis.cache.service.cache.mapper;

import com.demo.redis.cache.service.cache.dto.RoleDto;
import com.demo.redis.cache.service.storage.dto.UserDto;
import org.springframework.stereotype.Component;

import java.util.Collection;
import java.util.List;
import java.util.stream.Collectors;

@Component
public class RoleCacheStorageMapper {

    public com.demo.redis.cache.service.storage.dto.RoleDto mapToStorageDto(com.demo.redis.cache.service.cache.dto.RoleDto cacheDto) {
        com.demo.redis.cache.service.storage.dto.RoleDto storageRoleDto = new com.demo.redis.cache.service.storage.dto.RoleDto();

        storageRoleDto.setId(cacheDto.getId());
        storageRoleDto.setName(cacheDto.getName());
        storageRoleDto.setUsers(cacheDto.getUsers().stream().map(userId -> UserDto.builder().id(userId).build()).collect(Collectors.toList()));

        return null;
    }

    public com.demo.redis.cache.service.cache.dto.RoleDto mapToCacheDto(com.demo.redis.cache.service.storage.dto.RoleDto storageDto) {
        com.demo.redis.cache.service.cache.dto.RoleDto cacheRoleDto = new com.demo.redis.cache.service.cache.dto.RoleDto();

        cacheRoleDto.setId(storageDto.getId());
        cacheRoleDto.setName(storageDto.getName());
        cacheRoleDto.setUsers(storageDto.getUsers().stream().map(UserDto::getId).collect(Collectors.toList()));

        return cacheRoleDto;
    }

    public List<com.demo.redis.cache.service.storage.dto.RoleDto> mapToStorageDtoList(Collection<com.demo.redis.cache.service.cache.dto.RoleDto> cacheDtoList) {
        return cacheDtoList.stream().map(this::mapToStorageDto).collect(Collectors.toList());
    }

    public List<com.demo.redis.cache.service.cache.dto.RoleDto> mapToCacheDtoList(Collection<com.demo.redis.cache.service.storage.dto.RoleDto> storageDtoList) {
        return storageDtoList.stream().map(this::mapToCacheDto).collect(Collectors.toList());
    }
}
