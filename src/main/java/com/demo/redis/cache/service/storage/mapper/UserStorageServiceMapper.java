package com.demo.redis.cache.service.storage.mapper;

import com.demo.redis.cache.persistence.storage.entity.Role;
import com.demo.redis.cache.persistence.storage.entity.User;
import com.demo.redis.cache.service.storage.dto.RoleDto;
import com.demo.redis.cache.service.storage.dto.UserDto;
import org.springframework.stereotype.Component;

import java.util.*;
import java.util.stream.Collectors;

@Component
public class UserStorageServiceMapper {

	public User mapToEntity(final UserDto dto) {
		final User user = new User();

		if (null != dto) {
			if (null == dto.getId() || dto.getId() <= 0) {
				user.setId(null);
			} else {
				user.setId(dto.getId());
			}

			user.setName(dto.getName());
			user.setUsername(dto.getUsername());
			user.setPassword(dto.getPassword());
			user.setMail(dto.getMail());
			user.setStatus(dto.getStatus());
			user.setRoles(mapRolesEntity(dto.getRoles()));
		}

		return user;
	}

	public UserDto mapToDto(final User user) {
		final UserDto dto = new UserDto();

		if (null != user) {
			dto.setId(user.getId());
			dto.setName(user.getName());
			dto.setUsername(user.getUsername());
			dto.setPassword(user.getPassword());
			dto.setMail(user.getMail());
			dto.setStatus(user.getStatus());
			dto.setRoles(mapRolesDto(user.getRoles()));
		}
		return dto;
	}

	private Set<Role> mapRolesEntity(final List<RoleDto> roles) {
		final Set<Role> roleList = new HashSet<>();

		if (null != roles && !roles.isEmpty()) {
			for (final RoleDto role : roles) {
				final Role roleEntity = new Role();
				roleEntity.setId(role.getId());
				roleEntity.setName(role.getName());
				roleList.add(roleEntity);
			}

		}
		return roleList;
	}

	private List<RoleDto> mapRolesDto(final Set<Role> roles) {
		final List<RoleDto> roleList = new ArrayList<>();

		if (null != roles && !roles.isEmpty()) {
			for (final Role role : roles) {
				final RoleDto roleEntity = new RoleDto();
				roleEntity.setId(role.getId());
				roleEntity.setName(role.getName());
				roleList.add(roleEntity);
			}

		}
		return roleList;
	}

	public Set<User> mapToEntityList(final Collection<UserDto> dtoList) {
		if (dtoList == null) {
			return Collections.emptySet();
		}

		return dtoList.stream().map(this::mapToEntity).collect(Collectors.toSet());
	}

	public List<UserDto> mapToDtoList(final Collection<User> users) {
		if (users == null) {
			return Collections.emptyList();
		}

		return users.stream().map(this::mapToDto).collect(Collectors.toList());
	}

}
