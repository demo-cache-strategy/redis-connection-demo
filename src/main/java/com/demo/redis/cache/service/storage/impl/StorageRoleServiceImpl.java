package com.demo.redis.cache.service.storage.impl;

import com.demo.redis.cache.exception.BadRequestException;
import com.demo.redis.cache.exception.NotFoundException;
import com.demo.redis.cache.exception.error.CacheError;
import com.demo.redis.cache.persistence.storage.entity.Role;
import com.demo.redis.cache.persistence.storage.entity.User;
import com.demo.redis.cache.persistence.storage.repository.StorageRoleRepository;
import com.demo.redis.cache.persistence.storage.repository.StorageUserRepository;
import com.demo.redis.cache.service.storage.StorageRoleService;
import com.demo.redis.cache.service.storage.dto.RoleDto;
import com.demo.redis.cache.service.storage.dto.UserDto;
import com.demo.redis.cache.service.storage.mapper.RoleStorageServiceMapper;
import com.demo.redis.cache.util.constant.CommonConstants;
import com.demo.redis.cache.util.constant.ExceptionConstants;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Collections;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

@Service
@RequiredArgsConstructor
public class StorageRoleServiceImpl implements StorageRoleService {

	private final StorageRoleRepository storageRoleRepository;
	private final StorageUserRepository storageUserRepository;
	private final RoleStorageServiceMapper roleStorageServiceMapper;

	@Override
	public List<RoleDto> findAll() {
		final List<Role> roles = storageRoleRepository.findAll();
		return roleStorageServiceMapper.mapToDtoList(roles);
	}

	@Transactional(readOnly = true)
	@Override
	public RoleDto findById(final Integer id) {
		if (null == id) {
			throw new BadRequestException(new CacheError(ExceptionConstants.MESSAGE_BAD_REQUEST, ExceptionConstants.MESSAGE_BAD_REQUEST));
		}

		final Role role = storageRoleRepository.findById(id)
				.orElseThrow(() -> new NotFoundException("404", CommonConstants.ROLE_NOT_FOUND));
		return roleStorageServiceMapper.mapToDto(role);
	}

	@Transactional(readOnly = true)
	@Override
	public RoleDto findByName(final String name) {
		if (null == name) {
			throw new BadRequestException(new CacheError(ExceptionConstants.MESSAGE_BAD_REQUEST, ExceptionConstants.MESSAGE_BAD_REQUEST));
		}

		final Role role = storageRoleRepository.findByName(name)
				.orElseThrow(() -> new NotFoundException("404", CommonConstants.ROLE_NOT_FOUND));
		return roleStorageServiceMapper.mapToDto(role);
	}

	@Transactional
	@Override
	public RoleDto save(final RoleDto roleDto) {
		if (null == roleDto) {
			throw new BadRequestException(new CacheError(ExceptionConstants.MESSAGE_BAD_REQUEST, ExceptionConstants.MESSAGE_BAD_REQUEST));
		}

		Role role = new Role();
		role.setName(roleDto.getName());
		role.setUsers(null);

		role = storageRoleRepository.save(role);

		if (null != roleDto.getUsers() && !roleDto.getUsers().isEmpty()) {
			role = saveRoleInUser(roleDto, role);
		}

		return roleStorageServiceMapper.mapToDto(role);
	}

	@Transactional
	@Override
	public RoleDto update(final RoleDto roleDto) {
		if (null == roleDto || null == roleDto.getId()) {
			throw new BadRequestException(new CacheError(ExceptionConstants.MESSAGE_BAD_REQUEST, ExceptionConstants.MESSAGE_BAD_REQUEST));
		}

		Role role = storageRoleRepository.findById(roleDto.getId()).orElseThrow(() -> new NotFoundException("404", CommonConstants.ROLE_NOT_FOUND));

		role.setName(roleDto.getName());

		role = storageRoleRepository.save(role);

		if (!roleDto.getUsers().isEmpty()) {
			role = saveRoleInUser(roleDto, role);
		}

		return roleStorageServiceMapper.mapToDto(role);
	}

	private Role saveRoleInUser(final RoleDto roleDto, final Role role) {
		final Set<User> users = getUsers(roleDto.getUsers());

		users.forEach(user -> {
			if (!user.hasRole(role.getId())) {
				user.getRoles().add(role);
			}
		});

		storageUserRepository.saveAll(users);

		role.setUsers(users);

		return role;
	}

	private Set<User> getUsers(final List<UserDto> users) {
		if (null == users || users.isEmpty()) {
			return Collections.emptySet();
		}

		final Set<User> entityUsers = new HashSet<>();

		for (final UserDto dtoUser : users) {
			if (null != dtoUser) {
				final User entityUser = storageUserRepository.findById(dtoUser.getId()).orElse(null);
				if (null != entityUser) {
					entityUsers.add(entityUser);
				}
			}
		}

		return entityUsers;
	}

	@Transactional
	@Override
	public RoleDto delete(final RoleDto roleDto) {
		if (null == roleDto || null == roleDto.getId()) {
			throw new BadRequestException(new CacheError(ExceptionConstants.MESSAGE_BAD_REQUEST, ExceptionConstants.MESSAGE_BAD_REQUEST));
		}

		final Role role = storageRoleRepository.findById(roleDto.getId()).orElseThrow(() -> new NotFoundException("404", CommonConstants.ROLE_NOT_FOUND));

		role.getUsers().stream().forEach(user -> {
			user.setRoles(
					user.getRoles().stream().filter(x -> x.getId() != roleDto.getId()).collect(Collectors.toSet()));
			storageUserRepository.save(user);
		});

		storageRoleRepository.deleteById(roleDto.getId());
		return roleDto;
	}

}
