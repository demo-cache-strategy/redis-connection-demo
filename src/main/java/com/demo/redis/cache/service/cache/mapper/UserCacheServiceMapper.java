package com.demo.redis.cache.service.cache.mapper;

import com.demo.redis.cache.persistence.cache.entity.User;
import com.demo.redis.cache.service.cache.dto.UserDto;
import org.springframework.stereotype.Component;

import java.util.Collection;
import java.util.List;
import java.util.stream.Collectors;

@Component
public class UserCacheServiceMapper {

	public UserDto mapToDto(final User entity) {
		if (null == entity) {
			return null;
		}

		final UserDto dto = new UserDto();

		dto.setId(entity.getId());
		dto.setName(entity.getName());
		dto.setUsername(entity.getUsername());
		dto.setPassword(entity.getPassword());
		dto.setMail(entity.getMail());
		dto.setStatus(entity.getStatus());
		dto.setRoles(entity.getRoles());

		return dto;
	}

	public User mapToEntity(final UserDto dto) {
		if (null == dto) {
			return null;
		}

		final User entity = new User();

		entity.setId(dto.getId());
		entity.setName(dto.getName());
		entity.setUsername(dto.getUsername());
		entity.setPassword(dto.getPassword());
		entity.setMail(dto.getMail());
		entity.setStatus(dto.getStatus());
		entity.setRoles(dto.getRoles());

		return entity;
	}

	public List<UserDto> mapToDtoList(final Collection<User> entityList) {
		return entityList.stream().map(this::mapToDto).collect(Collectors.toList());
	}

	public List<User> mapToEntityList(final Collection<UserDto> dtpList) {
		return dtpList.stream().map(this::mapToEntity).collect(Collectors.toList());
	}

}
