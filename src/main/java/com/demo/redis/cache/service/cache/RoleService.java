package com.demo.redis.cache.service.cache;

import com.demo.redis.cache.service.cache.dto.RoleDto;

import java.util.List;

public interface RoleService {

    List<RoleDto> findAllRoles();
    RoleDto findById(RoleDto roleDto);
    RoleDto findByName(RoleDto roleDto);
    RoleDto save(RoleDto roleDto);
    RoleDto delete(RoleDto roleDto);

}
