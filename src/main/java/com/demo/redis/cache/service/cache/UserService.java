package com.demo.redis.cache.service.cache;

import com.demo.redis.cache.service.cache.dto.UserDto;

import java.util.List;

public interface UserService {
    List<UserDto> findAll();

    UserDto findById(UserDto userDto);

    UserDto findByName(UserDto userDto);

    UserDto findByUsername(UserDto userDto);

    UserDto save(UserDto userDto);
}
