package com.demo.redis.cache.service.cache.mapper;

import com.demo.redis.cache.persistence.cache.entity.Role;
import com.demo.redis.cache.service.cache.dto.RoleDto;
import org.springframework.stereotype.Component;

import java.util.Collection;
import java.util.List;
import java.util.stream.Collectors;

@Component
public class RoleCacheServiceMapper {

    public RoleDto mapToDto(final Role entity) {
        if (null == entity) {
            return null;
        }

        final RoleDto dto = new RoleDto();
        dto.setId(entity.getId());
        dto.setName(entity.getName());
        dto.setUsers(entity.getUsers());

        return dto;
    }

    public Role mapToEntity(final RoleDto dto) {
        if (null == dto) {
            return null;
        }

        final Role entity = new Role();
        entity.setId(dto.getId());
        entity.setName(dto.getName());
        entity.setUsers(dto.getUsers());

        return entity;
    }

    public List<Role> mapToEntityList(final Collection<RoleDto> dtoList) {
        return dtoList.stream().map(this::mapToEntity).collect(Collectors.toList());
    }

    public List<RoleDto> mapToDtoList(final Collection<Role> entityList) {
        return entityList.stream().map(this::mapToDto).collect(Collectors.toList());
    }

}
