package com.demo.redis.cache.persistence.cache.repository;

import com.demo.redis.cache.persistence.cache.entity.User;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public interface UserRepository  extends CrudRepository<User, String> {
    Optional<User> findByName(String name);
    Optional<User> findByUsername(String username);
}
