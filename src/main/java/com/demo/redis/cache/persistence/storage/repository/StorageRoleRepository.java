package com.demo.redis.cache.persistence.storage.repository;

import com.demo.redis.cache.persistence.storage.entity.Role;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public interface StorageRoleRepository extends JpaRepository<Role, Integer> {

	Optional<Role> findByName(String name);

}
