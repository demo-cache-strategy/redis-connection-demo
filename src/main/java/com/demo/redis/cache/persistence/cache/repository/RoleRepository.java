package com.demo.redis.cache.persistence.cache.repository;

import com.demo.redis.cache.persistence.cache.entity.Role;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public interface RoleRepository extends CrudRepository<Role, String> {
    Optional<Role> findByName(String name);
}
