package com.demo.redis.cache.persistence.cache.entity;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.data.redis.core.RedisHash;
import org.springframework.data.redis.core.index.Indexed;

import java.io.Serializable;
import java.util.List;

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
@RedisHash("User")
public class User implements Serializable {
    @Indexed
    private Integer id;
    @Indexed
    private String name;
    @Indexed
    private String username;
    private String password;
    private String status;
    private String mail;
    private List<Integer> roles;
}
