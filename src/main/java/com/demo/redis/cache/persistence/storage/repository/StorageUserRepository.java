package com.demo.redis.cache.persistence.storage.repository;

import com.demo.redis.cache.persistence.storage.entity.User;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface StorageUserRepository extends JpaRepository<User, Integer> {
}
