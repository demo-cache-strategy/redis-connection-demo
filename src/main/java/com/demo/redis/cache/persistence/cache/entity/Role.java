package com.demo.redis.cache.persistence.cache.entity;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.data.redis.core.RedisHash;
import org.springframework.data.redis.core.index.Indexed;

import java.util.List;

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
@RedisHash("Role")
public class Role {
    @Indexed
    private Integer id;
    @Indexed
    private String name;
    private List<Integer> users;
}
